package alias

object Foo {
  def apply(i: Int): Box[Int] = Box(i)
}

case class Box[A](a: A)
