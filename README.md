Scala Type Alias Bug
====================
Minimal reproduction of a type alias bug that interrupts class loading??? in a very strange manner.

reported here: https://issues.scala-lang.org/browse/SI-9025

Symptom
-------
When object is of the same name as a type alias in the same package, object can only be used once. 

For instance:

    scala> import alias._
    import alias._

    scala> Foo(1)
    res0: alias.Box[Int] = Box(1)

    scala> Foo(1)
    <console>:11: error: alias.Foo.type does not take parameters
                  Foo(1)
                     ^

Expected
--------
Either compilation fails, or you can use the object the second time, same as the first.
Also, some useful error message, the actual behaviour has very little to do with the cause.

Reproducible
------------
Always, under both Scala 2.10.4 and 2.11.4

To reproduce simply run sbt console and then the above.

Notes
-----
The type alias is otherwise unused.

Workaround
----------
Declare the object inside a trait and have the package object extend the trait
